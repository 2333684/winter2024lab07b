public class Card {
	private Suit suits;
	private Rank values;
 
	public Card (Suit suits, Rank values)
	{
		this.suits = suits;
		this.values = values;
	}
	
	public Suit getSuit()
	{
		return this.suits;
	}
	public Rank getValue()
	{
		return this.values;
	}
	
	public String toString()
	{
		return this.values + " of " + this.suits;
	}
	
	public double calculateScore()
	{		
		return suits.getScore() + values.getScore();
	}
	
}