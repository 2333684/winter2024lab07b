import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck()
	{
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[this.numberOfCards];
	
		int i = 0;
		for(Suit shape : Suit.values()){
			for(Rank value : Rank.values()) {
				cards[i] = new Card(shape, value);
				i++;
			}
		}
		
	}
	
	public int length()
	{
		return numberOfCards;
	}
	
	public Card drawTopCard()
	{
		Card topCard = cards[this.numberOfCards-1];
		Card [] placeHolder = this.cards;
		this.numberOfCards--;
		this.cards = new Card[this.numberOfCards];
		
		for(int i = 0; i < cards.length; i++){
			cards[i] = placeHolder[i];
		}
		return topCard;
	}
	
	public String toString()
	{
		String cardInDeck ="";
		
		for(Card c : this.cards){
			cardInDeck+= c + "\n";
		}
		return cardInDeck;
	}
	
	public void shuffle()
	{
		for (int i = 0; i < cards.length-1;i++){
			int randomIndex = rng.nextInt(numberOfCards);
			Card currentCard = cards[i];
			Card cardSwap = cards[randomIndex];
			cards[i] = cardSwap;
			cards[randomIndex] = currentCard;
		}
		
	}		
}
	
		